export class ChaosCondition {
    
    private isUpdateReady: boolean;
    
    private isSynchCompleted: boolean;
    
    private isCacheEnabled: boolean;
    
    private updateDb(isForceUpdate: boolean) {
        //  isUpdateReady is class level
        //  variable
        if (this.isUpdateReady) {
            //  isForceUpdate is argument variable
            //  and based on this inner blocks is
            //  executed
            if (isForceUpdate) {
                //  isSynchCompleted is also class
                //  level variable, based on its
                //  true/false updateDbMain is called
                //  here updateBackupDb is called
                //  in both the cases
                if (this.isSynchCompleted) {
                    this.updateDbMain(true);
                    this.updateBackupDb(true);
                }
                else {
                    this.updateDbMain(false);
                    this.updateBackupDb(true);
                }
                
            }
            else {
                //  execute this if isUpdateReady is
                //  false i. e., this is dependent on
                //  if condition
                this.updateCache(!this.isCacheEnabled);
                //  end of second isForceUpdate block
            }
            
            //  end of first if block
        }
        
        //  end of method
    }
    
    private updateCache(b: boolean) {
        
    }
    
    private updateBackupDb(b: boolean) {
        
    }
    
    private updateDbMain(b: boolean) {
        
    }
}