export class GoodParent {
    
    public findAllGoodMan(persons: List<Person>): List<Person> {
        let results = new ArrayList<Person>();
        for (let person: Person in persons) {
            if ((person.name.startsWith("T") 
                        && ((person.age > 25) 
                        && ((person.hairColor == HairColor.BLACK) 
                        && (person.skinColor == SkinColor.WHITE))))) {
                if ((person.sex == Gender.MALE)) {
                    results.add(person);
                }
                
            }
            
        }
        
        return results;
    }
    
    public findAllGoodGirl(persons: List<Person>): List<Person> {
        let results = new ArrayList<Person>();
        for (let person: Person in persons) {
            if ((person.name.startsWith("T") 
                        && ((person.age > 25) 
                        && ((person.hairColor == HairColor.BLACK) 
                        && (person.skinColor == SkinColor.WHITE))))) {
                if ((person.sex == Gender.FEMALE)) {
                    results.add(person);
                }
                
            }
            
        }
        
        return results;
    }
}
class Person {
    
    name: String;
    
    age: number;
    
    sex: Gender;
    
    hairColor: HairColor;
    
    skinColor: SkinColor;
}
enum SkinColor {
    
    BLACK,
    
    BROWN,
    
    WHITE,
}
enum HairColor {
    
    RED,
    
    BLACK,
    
    YELLOW,
}
enum Gender {
    
    FEMALE,
    
    MALE,
    
    GAY,
    
    LES,
}