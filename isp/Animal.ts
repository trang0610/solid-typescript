export interface Animal {

    walk();

    fly();
}
