import {Animal} from "./Animal";

export class Bird implements Animal {

    public walk() {
        console.log("bird can walk");
    }

    public fly() {
        console.log("bird can fly");
    }
}
